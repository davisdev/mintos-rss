## Installation
1. Clone the repo.
2. `cp .env.example .env`
3. `composer install`
4. `php artisan key:generate`
5. `npm install && npm run dev`
6. Create a database and put its credentials in `.env`
7. `php artisan migrate` (optionally add `--seed` flag, serves no real purpose)
8. `php artisan serve` to run the server
9. `cd <project-folder> && sudo chmod +x scheduler.sh && ./scheduler.sh` to run scheduler for feed updates
10. Make sure to pass in Pusher app key (make an app on pusher)
11. `php artisan websockets:serve` to run websockets server for real-time updates in feed
12. Register OR run `php artisan test-user:create` to make a test user and login with the presented credentials in console.

### Why am I using web sockets?
Mostly for UX purposes - no need to refresh the page, feed will update if websockets server and scheduler is running.

### Why no Docker?
Mostly due to test task deadline, compensating with code itself, otherwise would run all the necessary commands through docker containers with Supervisor.
