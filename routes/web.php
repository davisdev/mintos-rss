<?php

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index');

    Route::get('/rss', 'RssFeedController@index')->name('rss.index');
});

Route::post('/check-email', '\App\Http\Controllers\Auth\RegisterController@checkEmail')->name('register.check-email');
