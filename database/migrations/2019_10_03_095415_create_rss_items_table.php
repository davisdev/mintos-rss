<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRssItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rss_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('rss_feed_id');

            $table->string('rss_item_id');
            $table->text('title');
            $table->text('url');
            $table->text('summary')->nullable();

            $table->foreign('rss_feed_id')
                  ->references('id')
                  ->on('rss_feeds');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rss_items');
    }
}
