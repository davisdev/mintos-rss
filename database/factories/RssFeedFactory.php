<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RssFeed;
use App\Models\RssItem;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(RssFeed::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'subtitle' => $faker->sentence,

        // Feed might fail to update right after seeding since RSS feed
        // on external service gets updated not that frequently which might
        // mean that latest feeds results are, for example, from 4h ago.
        'feed_updated' => Carbon::parse($faker->time())->subDay(),
    ];
});

$factory->define(RssItem::class, function (Faker $faker) {
   return [
       'rss_feed_id' => $faker->numberBetween(1, 5), // overwriting in seeder
       'rss_item_id' => $faker->numberBetween(1, 1000),
       'title' => $faker->sentence,
       'url' => $faker->url,
       'summary' => $faker->sentence(30),
   ];
});
