<?php

use App\Models\RssFeed;
use App\Models\RssItem;
use Illuminate\Database\Seeder;

class RssFeedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $feeds = factory(RssFeed::class)->create();

        $feeds->each(function ($feed) {
            $feed->items()->saveMany(factory(RssItem::class, 20)->create([
                'rss_feed_id' => $feed->id,
            ]));
        });
    }
}
