@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">RSS Feed</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <feed></feed>
                </div>
            </div>
            <div class="card mt-2">
                <div class="card-header">Most used words in RSS feed</div>

                <div class="card-body">
                    <feed-statistics></feed-statistics>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
