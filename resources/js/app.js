/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Provide URL for any ajax.
 */
function currentUrl() {
    let protocol = window.location.protocol
    let host = window.location.hostname
    let port = window.location.port !== 80 ? ':' + window.location.port : ''

    return protocol + '//' + host + port
}

window.ajaxUrl = currentUrl()

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// For e-mail validation.
let emailFormat = new RegExp("[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")

import Echo from 'laravel-echo'
window.Pusher = require('pusher-js')

let pusherKey = process.env.MIX_PUSHER_APP_KEY

const app = new Vue({
    el: '#app',
    data: {
        email: {
            value: null,
            error: null
        },
        echo: new Echo({
            broadcaster: 'pusher',
            key: pusherKey,
            wsHost: window.location.hostname,
            wsPort: 6001,
            disableStats: true,
        })
    },

    methods: {
        /**
         * Check if e-mail already exists in system.
         *
         * @param email
         */
        checkEmailExistence(email) {
            if (! this.isLegitEmailAddress(this.email.value)) {
                this.email.error = "Please, provide a proper e-mail address"

                return
            }

            this.checkEmail()

            this.clearError()
        },

        /**
         * Validate if e-mail is correctly formatted.
         *
         * @param email
         * @returns {boolean}
         */
        isLegitEmailAddress(email) {
            return emailFormat.test(email)
        },

        /**
         * Clear e-mail errors.
         */
        clearError() {
            if (this.email.error) {
                this.email.error = null
            }
        },

        checkEmail() {
            window.axios.post(ajaxUrl + '/check-email', {
                email: this.email.value
            }).then(response => console.log(response))
                .catch(error => this.registerFormError(error))
        },

        registerFormError(error) {
            this.email.error = error.response.data.errors.email[0]
        }
    }
});
