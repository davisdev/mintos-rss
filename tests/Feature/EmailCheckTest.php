<?php

namespace Tests\Feature;

use App\Http\Middleware\VerifyCsrfToken;
use App\Models\User;
use Illuminate\Validation\ValidationException;
use Tests\RssBaseTest;

class EmailCheckTest extends RssBaseTest
{
    /**
     * @test
     */
    public function itShouldReturnErrorIfUsedEmailAddressIsInputtedInRegisterForm()
    {
        $user = factory(User::class)->create();
        $this->withMiddleware('guest');
        $this->withoutMiddleware(VerifyCsrfToken::class);
        $this->assertGuest();

        $this->expectException(ValidationException::class);

        $this->post('/check-email', [
           'email' => $user->email,
        ]);
    }
}
