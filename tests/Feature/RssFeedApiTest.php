<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\RssBaseTest;

class RssFeedApiTest extends RssBaseTest
{
    /**
     * @test
     */
    public function itShouldShowRssFeedItems()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $this->get('/rss')
             ->assertSuccessful()
             ->assertStatus(200)
             ->assertJson([
                 'data' => [],
             ]);
    }
}
