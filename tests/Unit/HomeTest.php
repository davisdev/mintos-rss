<?php

namespace Tests\Unit;

use App\Models\User;
use Tests\RssBaseTest;

class HomeTest extends RssBaseTest
{
    /**
     * @test
     */
    public function itShouldShowHomepageAfterSuccessfulLogin()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $this->get('/')
            ->assertStatus(200)
            ->assertSee('RSS Feed');
    }
}
