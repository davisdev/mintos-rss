<?php

namespace Tests\Unit;

use App\Models\RssFeed;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\RssBaseTest;

class RssFeedScheduleTest extends RssBaseTest
{
    use WithFaker;

    /**
     * @var
     */
    protected $dummyFeed;

    /**
     * @var
     */
    protected $dummyFeedFuture;

    public function setUp(): void
    {
        parent::setUp();

        // Dummy feeds needed for mostly all assertions
        $this->dummyFeed = $this->convertDummyFeedToXml('tests/testfiles/dummy_feed.xml');
        $this->dummyFeedFuture = $this->convertDummyFeedToXml('tests/testfiles/dummy_feed_future.xml');
    }

    /**
     * @test
     */
    public function itShouldSuccessfullyRunCommand()
    {
        $this->artisan('rss:parse')
            ->assertExitCode(0);
    }

    /**
     * @test
     */
    public function itShouldPopulateDatabaseWithRssFeedAndItemsForUniqueIncomingFeed()
    {
        $feed = (new RssFeed)->createFeed($this->dummyFeedFuture);

        $this->assertEquals(1, $feed->count());
        $this->assertDatabaseHas('rss_feeds', $feed->toArray());
    }

    /**
     * @test
     */
    public function itShouldParseIncomingFeedIfItIsUnique()
    {
        // Create dummy RSS feed dating in past (older than dummy file)
        $feed = factory(RssFeed::class)->create([
            'feed_updated' => Carbon::parse($this->faker->time())->subYear(),
        ]);

        $this->assertTrue($feed->containsNewData($this->dummyFeed));
    }

    /**
     * @test
     */
    public function itShouldNotParseIncomingFeedIfItIsNotUniqueButInPast()
    {
        $feed = factory(RssFeed::class)->create([
           'feed_updated' => Carbon::parse($this->faker->time())->addDay(),
        ]);

        $this->assertFalse($feed->containsNewData($this->dummyFeed));
    }

    /**
     * @test
     */
    public function itShouldNotParseFeedWhichHasSameUpdatedAtTimestampAsLatestFeed()
    {
        $updatedAtInDummyFeed = Carbon::parse($this->dummyFeed->updated_at);

        $feed = factory(RssFeed::class)->create([
           'feed_updated' =>  $updatedAtInDummyFeed
        ]);

        $this->assertFalse($feed->containsNewData($this->dummyFeed));
    }

    /**
     * @param $pathToDummyFile
     *
     * @return \SimpleXMLElement
     */
    private function convertDummyFeedToXml($pathToDummyFile)
    {
        return simplexml_load_string(
            file_get_contents(base_path($pathToDummyFile))
        );
    }
}
