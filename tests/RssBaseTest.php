<?php

namespace Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;

class RssBaseTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        // In order to see normal exceptions.
        $this->withoutExceptionHandling();
    }
}