<?php

namespace App\Providers;

use App\Models\RssFeed;
use App\Observers\RssFeedObserver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Cache considerations on model creation,
        // relational collection deletion etc.
        RssFeed::observe(RssFeedObserver::class);

        // For "interesting" cases...
        Schema::defaultStringLength(191);
    }
}
