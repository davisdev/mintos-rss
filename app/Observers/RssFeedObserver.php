<?php

namespace App\Observers;

use App\Models\RssFeed;

class RssFeedObserver
{
    /**
     * Handle the rss feed "created" event.
     *
     * @param  RssFeed  $rssFeed
     *
     * @return void
     * @throws \Exception
     */
    public function created(RssFeed $rssFeed)
    {
        cache()->forget($rssFeed->cacheKeys['statistics']);
    }

    /**
     * Handle the rss feed "deleted" event.
     *
     * @param  RssFeed  $rssFeed
     *
     * @return void
     * @throws \Exception
     */
    public function deleted(RssFeed $rssFeed)
    {
        $rssFeed->items()->delete();
        cache()->forget($rssFeed->cacheKeys['statistics']);
    }
}
