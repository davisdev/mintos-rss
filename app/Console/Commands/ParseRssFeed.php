<?php

namespace App\Console\Commands;

use App\Models\RssFeed;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class ParseRssFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rss:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse RSS provided in the settings.';

    /**
     * Guzzle instance for calling external service.
     *
     * @var Client
     */
    protected $guzzle;

    /**
     * Define headers for XML response.
     *
     * @var array
     */
    protected $headers = [
      'Accept' => 'application/xml',
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->guzzle = new Client();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        // TODO Maybe place it in Kernel class as a condition
        if (!config()->has('services.rss.url')) {
            return false;
        }

        $response = $this->guzzle->request('GET', config('services.rss.url'), $this->headers ?: [])
                            ->getBody()
                            ->getContents();

        $xml = new \SimpleXMLElement($response);

        if (!$this->isUniqueFeed($xml)) {
            return false;
        }

        RssFeed::handleIncomingFeed($xml);
    }

    /**
     * Check to make sure such feed has not
     * yet been created.
     *
     * @param  \SimpleXMLElement  $feed
     *
     * @return bool
     */
    private function isUniqueFeed(\SimpleXMLElement $feed)
    {
        return RssFeed::where('feed_updated', Carbon::parse($feed->updated))->count() === 0;
    }
}
