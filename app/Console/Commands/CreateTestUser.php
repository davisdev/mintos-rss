<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class CreateTestUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test-user:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create test user in order to not go through registration flow, saving seconds.';

    /**
     * Credentials for test user.
     *
     * @var array
     */
    protected $credentials = [
        'email' => 'test@test.com',
        'password' => 'admin',
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($user = User::where('email', $this->credentials['email'])->first()) {
            $this->line("Test user has already been created!");
            $this->table(['E-mail', 'Password'], [ [$this->credentials['email'], $this->credentials['password']] ]);

            return $user;
        }

        $user = factory(User::class)->create([
            'email' => $this->credentials['email'],
            'password' => Hash::make($this->credentials['password'])
        ]);

        $this->line('Test user created!');
        $this->table(['E-mail', 'Password'], [ [$this->credentials['email'], $this->credentials['password']] ]);

        return $user;
    }
}
