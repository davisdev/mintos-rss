<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class RssItem extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'rss_item_id',
        'title',
        'url',
        'summary',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'rss_feed_id',
        'rss_item_id',
    ];

    /**
     * RSS feed item belongs to one feed.
     *
     * @return BelongsTo
     */
    public function feed()
    {
        return $this->belongsTo(RssFeed::class, 'rss_feed_id');
    }
}
