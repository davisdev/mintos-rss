<?php

namespace App\Models;

use App\Events\RssFeedUpdated;
use Carbon\Carbon;
use Illuminate\Broadcasting\BroadcastException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;

class RssFeed extends Model
{
    /**
     * Eager load RSS feed items along with the feed.
     *
     * @var array
     */
    protected $with = [
        'items',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'subtitle',
        'feed_updated',
    ];

    /**
     * @var array
     */
    public $cacheKeys = [
        'statistics' => 'feed_most_used_words',
    ];

    /**
     * @return array
     * @throws \Exception
     */
    public function getStatisticsAttribute()
    {
        return cache()->rememberForever('feed_most_used_words', function () {
            return $this->getMostUsedWordStatistics()->toArray();
        });
    }

    public function getFeedUpdatedAttribute($feedUpdated)
    {
        return Carbon::parse($feedUpdated);
    }

    /**
     * Handle newest poll response from RSS.
     *
     * @param  \SimpleXMLElement  $feed
     */
    public static function handleIncomingFeed(\SimpleXMLElement $feed)
    {
        $instance = new static;

        if (self::shouldCreateNewFeed($feed, $instance)) {
            $feed = $instance->createFeed($feed);

            try {
                Event::dispatch(new RssFeedUpdated(
                    Hash::make($feed->updated)
                ));

                return $feed;
            } catch (BroadcastException $e) {
                \Log::info('Cannot connect to Pusher! In order to receive realtime updates, make sure to run WebSockets server.');
            }
        } else {
            return false;
        }
    }

    /**
     * Determine if the newly polled feed is updated.
     *
     * @param  \SimpleXMLElement  $feed
     *
     * @return bool
     */
    public function containsNewData(\SimpleXMLElement $feed)
    {
        $latestFeed = $this::latest()->first();

        return $latestFeed->feed_updated->lt(Carbon::parse($feed->updated));
    }

    /**
     * @param  \SimpleXMLElement  $feed
     * @param  RssFeed  $instance
     *
     * @return bool
     */
    private static function shouldCreateNewFeed(\SimpleXMLElement $feed, RssFeed $instance): bool
    {
        return $instance::count() < 1 || ($instance::count() && $instance->containsNewData($feed));
    }

    /**
     * Transform incoming feed data into models.
     *
     * @param  \SimpleXMLElement  $data
     */
    public function createFeed(\SimpleXMLElement $data)
    {
        $feed = $this::create([
            'title'        => $data->title,
            'subtitle'     => $data->subtitle,
            'feed_updated' => Carbon::parse($data->updated),
        ]);

        // Picking out RSS feed items stored under 'entry'.
        foreach ($data->entry as $item) {
            $feed->items()->create([
                'rss_item_id' => $item->id,
                'title'       => $item->title,
                'url'         => $this->extractItemUrl($item),
                'summary'     => $this->extractItemSummary($item),
            ]);
        }

        return $feed;
    }

    /**
     * Get URL for single RSS feed item.
     *
     * @param  \SimpleXMLElement  $item
     *
     * @return string
     */
    private function extractItemUrl(\SimpleXMLElement $item)
    {
        return (string) $item->link->attributes()['href'];
    }

    /**
     * Get summary for single RSS feed item.
     *
     * @param  \SimpleXMLElement  $item
     *
     * @return mixed
     */
    private function extractItemSummary(\SimpleXMLElement $item)
    {
        return strip_tags(((array) $item)['summary']);
    }

    /**
     * @return mixed
     */
    public function getMostUsedWordStatistics()
    {
        $latestFeed = RssFeed::latest()->first();

        return $this->createWordStatistics($latestFeed->items);
    }

    /**
     * @param  Collection  $items
     *
     * @return mixed
     */
    private function createWordStatistics(Collection $items)
    {
        // Key-by together all the items to avoid any duplicate
        // deletion and merge titles and summaries together.
        $allUsedWords = $items->keyBy('id')->map(function ($item) {
            return implode(' ', [$item->title, $item->summary]);
        })->implode(' ');

        return collect(explode(' ', $allUsedWords))->map(function ($word) {
            return strtolower($this->stripSpecialChars($word));
        })->filter(function ($word) {
            return $this->isRealWord($word) && !$this->isCommonUsedWord($word);
        })->countBy()->sort()->reverse()->take(10);
    }

    /**
     * @param  string  $word
     *
     * @return Collection
     */
    private function stripSpecialChars(string $word)
    {
        return str_replace([':', '-', ',', '.', '!', '?', '/', '\'', '"', '(', ')'], '', $word);
    }

    /**
     * @return mixed
     */
    private function getMostUsedEnglishWords()
    {
        $filePath = resource_path('data/mostUsedWords.json');

        return json_decode(
            file_get_contents($filePath)
        )->words;
    }

    /**
     * Determine if the current word is real / 'a word'.
     *
     * @param  string  $word
     *
     * @return bool
     */
    private function isRealWord(string $word)
    {
        return !is_numeric($word) && preg_match('/[a-z]/i', $word);
    }

    /**
     * @param  string  $word
     *
     * @return bool
     */
    private function isCommonUsedWord(string $word)
    {
        return in_array($word, $this->getMostUsedEnglishWords());
    }

    /**
     * Feed has many RSS feed items.
     *
     * @return HasMany
     */
    public function items()
    {
        return $this->hasMany(RssItem::class);
    }
}
