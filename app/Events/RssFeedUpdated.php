<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class RssFeedUpdated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Unique feed ID by 'updated' timestamp.
     *
     * @var
     */
    protected $feedUid;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($uid)
    {
        $this->feedUid = $uid;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel
     */
    public function broadcastOn()
    {
        return new Channel('feed');
    }

    /**
     * Add more data for broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return [
            'hash' => $this->feedUid,
        ];
    }
}
