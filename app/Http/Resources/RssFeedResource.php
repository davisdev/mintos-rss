<?php

namespace App\Http\Resources;

use App\Models\RssFeed;
use Illuminate\Http\Resources\Json\ResourceCollection;

class RssFeedResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection
        ];
    }

    public function with($request)
    {
        return [
            'statistics' => $this->formatStatistics((new RssFeed)->statistics),
        ];
    }

    /**
     * @param $statistics
     *
     * @return array
     */
    private function formatStatistics($statistics)
    {
        $iteration = 0;
        $formattedStatistics = [];

        foreach ($statistics as $word => $times) {
            $formattedStatistics[$iteration] = [
                'word' => $word,
                'times_mentioned' => $times
            ];

            $iteration++;
        }

        return $formattedStatistics;
    }
}
