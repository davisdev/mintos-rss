<?php

namespace App\Http\Controllers;

use App\Http\Resources\RssFeedResource;
use App\Models\RssFeed;

class RssFeedController extends Controller
{
    /**
     * Normalized RSS feed for front-end.
     *
     * @return RssFeedResource
     */
    public function index()
    {
        $feed = RssFeed::latest()->first();

        if (!$feed) {
            return response()->json([
                'data' => []
            ]);
        }

        return new RssFeedResource($feed->items);
    }
}
